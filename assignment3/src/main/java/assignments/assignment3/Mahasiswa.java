package assignments.assignment3;

import java.util.Arrays;

class Mahasiswa extends ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private MataKuliah[] daftarMataKuliah = new MataKuliah[10];
    private int jumlahMataKuliah = 0;
    
    private long npm;

    private String tanggalLahir;
    
    private String jurusan;

    public Mahasiswa(String nama, long npm) {
        // constructor for Mahasiswa class
        super(nama);    // invokes constructor from superclass
        this.npm = npm;
        this.jurusan = extractJurusan(npm);
        this.tanggalLahir = extractTanggalLahir(npm);
        setTipe("Mahasiswa");
    }

    // getter methods
    public long getNpm() { return npm; }
    public int getJumlahMataKuliah() { return jumlahMataKuliah;}
    public MataKuliah[] getDaftarMatkul() { return daftarMataKuliah; }
    public String getTanggalLahir() { return tanggalLahir; }
    public String getJurusan() {return jurusan; }


     public void addMatkul(MataKuliah mataKuliah) {
        // case where there hasn't been any courses added
        if (daftarMataKuliah[0] == null ) {

            // only rejected when the course is at full capacity
            if (mataKuliah.getJumlahMahasiswa() == mataKuliah.getKapasitas()) {
                System.out.println("[DITOLAK] " + mataKuliah + " telah penuh kapasitasnya");
                return;
            }
            daftarMataKuliah[0] = mataKuliah;
            jumlahMataKuliah++;
            mataKuliah.addMahasiswa(this);  // add the student to the course
            System.out.printf("%s berhasil menambahkan mata kuliah %s\n", this.toString(), mataKuliah.toString());
            return;
        }


        if (Arrays.asList(daftarMataKuliah).contains(mataKuliah)) {  // check if student is already enrolled
            System.out.println("[DITOLAK] " + mataKuliah + " telah diambil sebelumnya");
        }
        else { // check if the capacity is enough
            if (mataKuliah.getJumlahMahasiswa() == mataKuliah.getKapasitas()) {
                System.out.println("[DITOLAK] " + mataKuliah + " telah penuh kapasitasnya");
            }
            else {
                if (jumlahMataKuliah == 10) {   // check if the number of courses taken is equal to 10
                    System.out.println("[DITOLAK] Maksimal mata kuliah yang diambil hanya 10");
                }
                else {
                    daftarMataKuliah[jumlahMataKuliah] = mataKuliah; // add to mataKuliah array
                    jumlahMataKuliah++;
                    mataKuliah.addMahasiswa(this); // add the student to the course
                    System.out.printf("%s berhasil menambahkan mata kuliah %s\n", this.toString(), mataKuliah.toString());
                }
            }
        }
    }

    public void dropMatkul(MataKuliah mataKuliah) {

        // checks if student is enrolled in the course or not
        boolean test = Arrays.asList(daftarMataKuliah).contains(mataKuliah);
        if (!test) {
            System.out.println("[DITOLAK] " +  mataKuliah + " belum pernah diambil");
            return;
        }

        MataKuliah[] temp = Arrays.copyOf(daftarMataKuliah, daftarMataKuliah.length);

        // if the course is in the middle of the list, shift the courses after to the left
        for (int i = 0, j = 0; i < jumlahMataKuliah; i++) {
            if (!temp[i].toString().equals(mataKuliah.toString())){
                daftarMataKuliah[j] = temp[i];
                j++;
            }
        }
        jumlahMataKuliah--;
        daftarMataKuliah[jumlahMataKuliah] = null;
        mataKuliah.dropMahasiswa(this); // remove student from course
        System.out.printf("%s berhasil drop mata kuliah %s", this.toString(), mataKuliah.toString());
    }

    public String extractTanggalLahir(long npm) {

        // use modulo operator to get the birthdate
        int birthYear, birthMonth, birthDate;
        birthYear = (int) ((npm / 100) % 10000);
        birthMonth = (int) ((npm / 1000000) % 100);
        birthDate = (int) ((npm / 100000000) % 100);
        return birthDate + "-" + birthMonth + "-" + birthYear;
    }


    public String extractJurusan(long npm) {
        // use modulo operator to get the major code
        int kodeJurusan = (int) ((npm / 10000000000L) % 100);
        if (kodeJurusan == 1)
            return "Ilmu Komputer";
        else
            return  "Sistem Informasi";
    }
}