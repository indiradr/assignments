package assignments.assignment3;



abstract class ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private String tipe;

    private String nama;

    private int friendship = 0;

    private ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];

    private int jumlahTelahMenyapa = 0;


    // constructor and getter setter methods
    public ElemenFasilkom(String nama) {
        this.nama = nama;
    }

    public String getTipe() { return tipe; }

    public void setTipe(String tipe) { this.tipe = tipe; }

    public int getJumlahTelahMenyapa() { return jumlahTelahMenyapa; }

    public void setJumlahTelahMenyapa(int n) { jumlahTelahMenyapa = n; }

    public ElemenFasilkom[] getTelahMenyapa() { return telahMenyapa; }

    public int getFriendship() { return friendship; }

    public void setFriendship(int friendship) {
        this.friendship = friendship;
        if (this.friendship < 0) { // make sure that the score is greater or equal to zero
            this.friendship = 0;
        }

        if (this.friendship > 100) { // make sure that the score is less or equal to 100
            this.friendship = 100;
        }
    }




    public void menyapa(ElemenFasilkom elemenFasilkom) {
        /* TODO: implementasikan kode Anda di sini */
        // method ini akan menyimpan orang yang disapa ke dalam array telahMenyapa.

        for (int i = 0; i < jumlahTelahMenyapa; i++) {
            if (telahMenyapa[i] == null) {
                break;
            }

            // case where they've already greet each other
            if (elemenFasilkom.toString().equals(telahMenyapa[i].toString())) {
                System.out.printf("[DITOLAK] %s telah menyapa %s hari ini\n", nama, elemenFasilkom.toString());
                return;
            }
        }

        telahMenyapa[jumlahTelahMenyapa] = elemenFasilkom;
        jumlahTelahMenyapa++;

        // if A greets B, then B greets A
        ElemenFasilkom[] jugaMenyapa = elemenFasilkom.getTelahMenyapa();
        jugaMenyapa[elemenFasilkom.getJumlahTelahMenyapa()] = this;
        elemenFasilkom.setJumlahTelahMenyapa(elemenFasilkom.getJumlahTelahMenyapa() + 1);

        System.out.printf("%s menyapa dengan %s\n", nama, elemenFasilkom.toString());

        // cases where a student greets a professor and vice versa
        if (this.getTipe().equals("Mahasiswa") && elemenFasilkom.getTipe().equals("Dosen")) {
            Mahasiswa mahasiswa = (Mahasiswa) this;
            Dosen dosen = (Dosen) elemenFasilkom;
            menyapaDosenMahasiswa(mahasiswa, dosen);

        } else if (this.getTipe().equals("Dosen") && elemenFasilkom.getTipe().equals("Mahasiswa")) {
            Mahasiswa mahasiswa = (Mahasiswa) elemenFasilkom;
            Dosen dosen = (Dosen) this;
            menyapaDosenMahasiswa(mahasiswa, dosen);

        }


    }

    public void resetMenyapa() {
        /* TODO: implementasikan kode Anda di sini */
        // method ini akan menghapus daftar orang yang telah disapa
        telahMenyapa = new ElemenFasilkom[100];
        jumlahTelahMenyapa = 0;
    }

    public void menyapaDosenMahasiswa(Mahasiswa mahasiswa, Dosen dosen) {
        MataKuliah[] daftarMatkul = mahasiswa.getDaftarMatkul();

        // when either the professor doesn't teach or the student isn't enlisted in any courses
        if (dosen.getMataKuliah() == null || daftarMatkul[0] == null) {
            return;
        }

        // find out if they're in the same course
        boolean terhubungMatakuliah = false;
        for (int i = 0; i < mahasiswa.getJumlahMataKuliah(); i++) {
            if (daftarMatkul[i].toString().equals(dosen.getMataKuliah().toString())) {
                terhubungMatakuliah = true;
                break;
            }
        }

        // the relationship between student and professor increase if they are in the same course
        if (terhubungMatakuliah) {
            mahasiswa.setFriendship(mahasiswa.getFriendship() + 2);
            dosen.setFriendship(dosen.getFriendship() + 2);
        }
    }

    public void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {


        Makanan makananDibeli = null;
        ElemenKantin penjualKantin = (ElemenKantin) penjual;    // downcast to ElemenKantin to access methods
        Makanan[] daftarMakanan = penjualKantin.getDaftarMakanan();

        // find out if the seller has the item
        for (int i = 0; i < penjualKantin.getJumlahMakanan(); i++) {
            if (namaMakanan.equals(daftarMakanan[i].toString())) {
                makananDibeli = daftarMakanan[i];
                break;
            }
        }

        if (makananDibeli != null) {
            System.out.printf("%s berhasil membeli %s seharga %d\n", pembeli.toString(), namaMakanan, makananDibeli.getHarga());
            pembeli.setFriendship(pembeli.getFriendship() + 1);
            penjual.setFriendship(penjual.getFriendship() + 1);
        }
        else { // the elemenKantin does not sell the item
            System.out.printf("[DITOLAK] %s tidak menjual %s\n", penjual.toString(), namaMakanan);
        }



    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return nama;
    }
}