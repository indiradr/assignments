package assignments.assignment3;

class Dosen extends ElemenFasilkom {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private MataKuliah mataKuliah = null;

    Dosen(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        super(nama); // invoke superclass constructor
        setTipe("Dosen");
    }

    // getter method(s)
    public MataKuliah getMataKuliah() { return mataKuliah; }


    public void mengajarMataKuliah(MataKuliah mataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        // method ini akan mendaftarkan objek Dosen ke dalam mataKuliah

        if (this.mataKuliah == null) {
            if (mataKuliah.getDosen() == null) {
                this.mataKuliah = mataKuliah;
                System.out.printf("%s mengajar mata kuliah %s\n", this.toString(), mataKuliah.toString());
                mataKuliah.addDosen(this);
            } else {
                // Jika dosen mendaftar ke mata kuliah yang sudah memiliki dosen pengajar
                System.out.printf("[DITOLAK] %s sudah memiliki dosen pengajar\n", mataKuliah.toString());
            }
        } else {
            // Jika ternyata objek Dosen telah mengajar suatu mataKuliah
            System.out.printf("[DITOLAK] %s sudah mengajar mata kuliah %s\n", this.toString(), this.mataKuliah.toString());
        }
    }

    public void dropMataKuliah() {
        /* TODO: implementasikan kode Anda di sini */
        // method ini akan melepaskan mata kuliah yang sedang dipegang

        if (mataKuliah != null) {
            System.out.printf("%s berhenti mengajar %s\n", this.toString(), mataKuliah.toString());
            mataKuliah.dropDosen();
            mataKuliah = null;

        } else {    // case where the dosen is not teaching any classes
            System.out.printf("[DITOLAK] %s sedang tidak mengajar mata kuliah apapun\n", this.toString());
        }
    }

}