package assignments.assignment3;

import java.util.Scanner;

public class Main {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private static ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];

    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];

    private static int totalMataKuliah = 0;

    private static int totalElemenFasilkom = 0;

    public static void addMahasiswa(String nama, long npm) {
        // create a new Mahasiswa object and add to daftarElemenFasilkom array
        daftarElemenFasilkom[totalElemenFasilkom] = new Mahasiswa(nama, npm);
        totalElemenFasilkom++;
        System.out.printf("%s berhasil ditambahkan\n", nama);
    }

    public static void addDosen(String nama) {
        // create a new Dosen object and add to daftarElemenFasilkom array
        daftarElemenFasilkom[totalElemenFasilkom] = new Dosen(nama);
        totalElemenFasilkom++;
        System.out.printf("%s berhasil ditambahkan\n", nama);
    }

    public static void addElemenKantin(String nama) {
        // create a new ElemenKantin object and add to daftarElemenFasilkom array
        daftarElemenFasilkom[totalElemenFasilkom] = new ElemenKantin(nama);
        totalElemenFasilkom++;
        System.out.printf("%s berhasil ditambahkan\n", nama);
    }

    public static void menyapa(String objek1, String objek2) {

        if (objek1.equals(objek2)) {
            System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");
            return;
        }

        ElemenFasilkom elemenFasilkom1 = getElemenFasilkom(objek1);
        ElemenFasilkom elemenFasilkom2 = getElemenFasilkom(objek2);

        if (elemenFasilkom1 != null && elemenFasilkom2 != null) {
            elemenFasilkom1.menyapa(elemenFasilkom2);
        }


    }

    public static void addMakanan(String objek, String namaMakanan, long harga) {

        ElemenFasilkom elemenFasilkom = getElemenFasilkom(objek);
        if (!elemenFasilkom.getTipe().equals("Elemen Kantin")) { // the object is not an elemen kantin
            System.out.printf("[DITOLAK] %s bukan merupakan elemen kantin\n", objek);
            return;
        }

        ElemenKantin elemenKantin = (ElemenKantin) elemenFasilkom;
        elemenKantin.setMakanan(namaMakanan, harga);


    }

    public static void membeliMakanan(String objek1, String objek2, String namaMakanan) {

        if (objek1.equals(objek2)) {
            System.out.println("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri");
            return;
        }

        ElemenFasilkom elemenFasilkom1 = getElemenFasilkom(objek1);
        ElemenFasilkom elemenFasilkom2 = getElemenFasilkom(objek2);

        if (!elemenFasilkom2.getTipe().equals("Elemen Kantin")) {
            System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan");
            return;
        }

        elemenFasilkom1.membeliMakanan(elemenFasilkom1, elemenFasilkom2, namaMakanan);



    }

    public static void createMatkul(String nama, int kapasitas) {
        // make new MataKuliah object and put into daftarMataKuliah array
        MataKuliah temp = new MataKuliah(nama, kapasitas);
        daftarMataKuliah[totalMataKuliah] = temp;
        totalMataKuliah++;
        System.out.printf("%s berhasil ditambahkan dengan kapasitas %d\n", nama, kapasitas);
    }

    public static void addMatkul(String objek, String namaMataKuliah) {

        ElemenFasilkom elemenFasilkom = getElemenFasilkom(objek);
        if (!elemenFasilkom.getTipe().equals("Mahasiswa")) { // when the object is not a Mahasiswa
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
            return;
        }

        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
        Mahasiswa mahasiswa = (Mahasiswa) elemenFasilkom; // downcast to Mahasiswa subclass to access methods
        mahasiswa.addMatkul(mataKuliah);

    }

    public static void dropMatkul(String objek, String namaMataKuliah) {

        ElemenFasilkom elemenFasilkom = getElemenFasilkom(objek);
        if (!elemenFasilkom.getTipe().equals("Mahasiswa")) { // when the object is not a Mahasiswa
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");
            return;
        }

        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
        Mahasiswa mahasiswa = (Mahasiswa) elemenFasilkom; // downcast to Mahasiswa subclass to access methods
        mahasiswa.dropMatkul(mataKuliah);

    }

    public static void mengajarMatkul(String objek, String namaMataKuliah) {

        ElemenFasilkom elemenFasilkom = getElemenFasilkom(objek);
        if (!elemenFasilkom.getTipe().equals("Dosen")) { // when the object is not a Mahasiswa
            System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");
            return;
        }

        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
        Dosen dosen = (Dosen) elemenFasilkom; // downcast to Mahasiswa subclass to access methods
        dosen.mengajarMataKuliah(mataKuliah);

    }

    public static void berhentiMengajar(String objek) {

        ElemenFasilkom elemenFasilkom = getElemenFasilkom(objek);
        if (!elemenFasilkom.getTipe().equals("Dosen")) { // when the object is not a Mahasiswa
            System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");
            return;
        }

        Dosen dosen = (Dosen) elemenFasilkom; // downcast to Mahasiswa subclass to access methods
        dosen.dropMataKuliah();

    }

    public static void ringkasanMahasiswa(String objek) {

        ElemenFasilkom elemenFasilkom = getElemenFasilkom(objek);
        if (!elemenFasilkom.getTipe().equals("Mahasiswa")) { // when the object is not a Mahasiswa
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");
            return;
        }
        Mahasiswa mahasiswa = (Mahasiswa) elemenFasilkom; // downcast to Mahasiswa subclass to access methods
        System.out.printf("Nama: %s\n", objek);
        System.out.printf("Tanggal lahir: %s\n", mahasiswa.getTanggalLahir());
        System.out.printf("Jurusan: %s\n", mahasiswa.getJurusan());
        System.out.println("Daftar Mata Kuliah:");

        MataKuliah[] daftarMatkul = mahasiswa.getDaftarMatkul();
        if (daftarMatkul[0] == null) {  // when no courses have been taken
            System.out.println("Belum ada matakuliah yang diambil");
        }

        else { // print course list
            for (int j = 0; j < mahasiswa.getJumlahMataKuliah(); j++) {
                System.out.printf("%d. %s\n", (j + 1), daftarMatkul[j]);
            }
        }



    }

    public static void ringkasanMataKuliah(String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */

        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
        System.out.println("Nama mata kuliah: " + namaMataKuliah);
        System.out.println("Jumlah mahasiswa: " + mataKuliah.getJumlahMahasiswa());
        System.out.println("Kapasitas: " + mataKuliah.getKapasitas());
        System.out.println("Dosen pengajar: " + (mataKuliah.getDosen() == null ?
                "Belum ada" : mataKuliah.getDosen().toString()));
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini:");

        if (mataKuliah.getJumlahMahasiswa() == 0) { // no student's in course
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini");
        }
        else {
            Mahasiswa[] daftarMahasiswa = mataKuliah.getDaftarMahasiswa();
            for (int j = 0; j < mataKuliah.getJumlahMahasiswa(); j++) {
                System.out.printf("%d. %s\n", (j + 1), daftarMahasiswa[j].toString());
            }
        }

    }

    public static void nextDay() {
        /* TODO: implementasikan kode Anda di sini */

        int tempFriendship;
        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate");
        for (int i = 0; i < totalElemenFasilkom; i++) {
            tempFriendship = 0;
            if (daftarElemenFasilkom[i].getJumlahTelahMenyapa() >= ((totalElemenFasilkom - 1) / 2)) {
                tempFriendship += 10;   // greet more than or equal to half of the daftarElemenFasilkom
            }
            else {
                tempFriendship -= 5;   // less than or equal to half of the daftarElemenFasilkom
            }
            daftarElemenFasilkom[i].setFriendship(daftarElemenFasilkom[i].getFriendship() + tempFriendship);

        }

        friendshipRanking();


    }

    public static void friendshipRanking() {
        /* TODO: implementasikan kode Anda di sini */

        // use bubble sort to sort the friendship ranking

        for (int i = 0; i < totalElemenFasilkom - 1; i++) {
            for (int j = 0; j < totalElemenFasilkom - i - 1; j++) {
                if (daftarElemenFasilkom[j].getFriendship() < daftarElemenFasilkom[j + 1].getFriendship()) {
                    // swap arr[j+1] and arr[j] when the friendship is less
                    ElemenFasilkom temp = daftarElemenFasilkom[j];
                    daftarElemenFasilkom[j] = daftarElemenFasilkom[j + 1];
                    daftarElemenFasilkom[j + 1] = temp;
                }

                // case where the friendship is the same, so sort lexicographically
                if (daftarElemenFasilkom[j].getFriendship() == daftarElemenFasilkom[j + 1].getFriendship()) {
                    if ((daftarElemenFasilkom[j].toString()).compareToIgnoreCase((daftarElemenFasilkom[j + 1]).toString()) > 0) {
                        // swap arr[j+1] and arr[j]
                        ElemenFasilkom temp = daftarElemenFasilkom[j];
                        daftarElemenFasilkom[j] = daftarElemenFasilkom[j + 1];
                        daftarElemenFasilkom[j + 1] = temp;
                    }
                }
            }
        }

        // print ranking
        for (int k = 0; k < totalElemenFasilkom; k++) {
            System.out.printf("%d. %s(%d)\n", (k + 1), (daftarElemenFasilkom[k].toString()), (daftarElemenFasilkom[k].getFriendship()));
            daftarElemenFasilkom[k].resetMenyapa();
        }
    }

    public static void programEnd() {
        // print the last ranking
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada Fasilkom :");
        friendshipRanking();

    }

    public static ElemenFasilkom getElemenFasilkom(String nama) {

        // iterates over the daftarElemenFasilkom array and returns the required elemenFasilkom
        for (int i = 0; i < totalElemenFasilkom; i++) {
            if (nama.equals(daftarElemenFasilkom[i].toString())) {
                return daftarElemenFasilkom[i];
            }
        }
        return null;
    }

    public static MataKuliah getMataKuliah(String nama) {
        // iterates over the daftarMataKuliah array and returns the required mataKuliah
        for (int i = 0; i < totalMataKuliah; i++) {
            if (nama.equals(daftarMataKuliah[i].toString())) {
                return daftarMataKuliah[i];
            }
        }
        return null;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                break;
            }
        }
    }
}