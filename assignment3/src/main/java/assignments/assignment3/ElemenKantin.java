package assignments.assignment3;

class ElemenKantin extends ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private Makanan[] daftarMakanan = new Makanan[10];
    private int jumlahMakanan = 0;

    public int getJumlahMakanan() {
        return jumlahMakanan;
    }

    public Makanan[] getDaftarMakanan() {
        return daftarMakanan;
    }


    ElemenKantin(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        super(nama);    // invokes constructor from superclass
        setTipe("Elemen Kantin");
    }


    public void setMakanan(String nama, long harga) {
        /* TODO: implementasikan kode Anda di sini */
        if (jumlahMakanan == 0) {
            Makanan tempMakanan = new Makanan(nama, harga);
            daftarMakanan[0] = tempMakanan;
            jumlahMakanan++;
            System.out.printf("%s telah mendaftarkan makanan %s dengan harga %d\n", this.toString(), nama, harga);
        }
        else {
            if (jumlahMakanan == 10) {   // check if the number of courses taken is equal to 10
                System.out.println("[DITOLAK] Maksimal makanan yang dapat didaftarkan hanya 10.");
            }
            boolean sudahAda = false;
            for (int i = 0; i < jumlahMakanan; i++) {
                if (nama.equals(daftarMakanan[i].toString())) {
                    sudahAda = true;
                    break;
                }
            }

            if (sudahAda) {
                System.out.printf("[DITOLAK] %s sudah pernah terdaftar\n", nama);
            }
            else {
                System.out.printf("%s telah mendaftarkan makanan %s dengan harga %s\n", this.toString(), nama, harga);
                Makanan tempMakanan = new Makanan(nama, harga);
                daftarMakanan[jumlahMakanan] = tempMakanan;
                jumlahMakanan++;
            }
        }
    }

}