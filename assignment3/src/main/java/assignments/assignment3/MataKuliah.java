package assignments.assignment3;

import java.util.Arrays;

class MataKuliah {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private String nama;
    private int kapasitas;
    private int jumlahMahasiswa = 0;
    private Dosen dosen = null;
    private Mahasiswa[] daftarMahasiswa;

    // getter methods
    public Dosen getDosen() {
        return dosen;
    }
    public int getKapasitas() { return kapasitas;}
    public int getJumlahMahasiswa() { return jumlahMahasiswa; }
    public Mahasiswa[] getDaftarMahasiswa() { return daftarMahasiswa;}

    public MataKuliah(String nama, int kapasitas) {
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.kapasitas = kapasitas;

        // the size of the array is dependent on the capacity of the course
        daftarMahasiswa = new Mahasiswa[kapasitas];
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        // checking if the student is eligible to take the course has already been checked in Mahasiswa class
        // so just add the student in the course
        daftarMahasiswa[jumlahMahasiswa] = mahasiswa;
        jumlahMahasiswa++;
    }

    void dropMahasiswa(Mahasiswa mahasiswa) {
        // // shift the student to the left if the dropped student is in the middle of array
        Mahasiswa[] temp = Arrays.copyOf(daftarMahasiswa, daftarMahasiswa.length);
        for (int i = 0, j = 0; i < jumlahMahasiswa; i++) {
            if (!temp[i].toString().equals(mahasiswa.toString())){
                daftarMahasiswa[j] = temp[i];
                j++;
            }

        }
        jumlahMahasiswa--;
        daftarMahasiswa[jumlahMahasiswa] = null;
    }

    public void addDosen(Dosen dosen) {
        // checking if the dosen is eligible to teach the course has already been checked in Dosen class
        // so just add the dosen to the course
        this.dosen = dosen;
    }

    void dropDosen() {
        // checking if the dosen is teaching the course has already been checked in Dosen class
        // so just drop the dosen to the course
        this.dosen = null;
    }

    public String toString() {
        return nama;
    }
}