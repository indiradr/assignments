package assignments.assignment3;

class Makanan {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    String nama;

    long harga;

    Makanan(String nama, long harga) {
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.harga = harga;
    }

    public long getHarga() {
        return harga;
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return nama;
    }
}