package assignments.assignment1;

import java.util.Scanner;

public class ExtractNPM {

    public static boolean validate(long npm) {
        // TODO: validate NPM, return it with boolean

        // 1. check: if the NPM has 14 digits
        int length = (int) (Math.log10(npm) + 1);       // finds the length of the npm
        if (length != 14) {
            return false;
        }

        // get each piece of data using modulo and division
        long yearAdmitted, majorCode, birthYear, birthMonth, birthDate, npmCode;   // different parts of NPM structure
        npmCode = npm % 10;                              // part E
        birthYear = (npm / 100) % 10000;                // part C
        birthMonth = (npm / 1000000) % 100;
        birthDate = (npm / 100000000) % 100;
        majorCode = (npm / 10000000000L) % 100;         // part B
        yearAdmitted = (npm / 1000000000000L) % 100;    // part A

        // 2. check: the major code is listed
        if ((majorCode != 1) && (majorCode != 2) && (majorCode != 3) && (majorCode != 11) && (majorCode != 12)) {
            return false;
        }

        // 3. check: the age is >= 15
        if ((2000 + yearAdmitted) - birthYear < 15) {
            return false;
        }

        // 4. check: tanggal lahir yang dimasukkan valid
        if (birthYear < 2000) {  // the year has to be in the 21st century
            return false;
        }

        boolean isLeapYear = false;     // for checking february's dates during leap year
        if (birthYear % 4 == 0) {
            if (birthYear % 100 == 0) {
                if (birthYear % 400 == 0) {
                    isLeapYear = true;
                }
            }
        }

        if ((birthMonth == 1) || (birthMonth == 3) || (birthMonth == 7) || (birthMonth == 8) || (birthMonth == 10) || (birthMonth == 12)) {
            if (birthDate < 1 || birthDate > 31) {      // checks for months that have 31 days
                return false;
            }
        } else if ((birthMonth == 2) || (birthMonth == 4) || (birthMonth == 6) || (birthMonth == 9) || (birthMonth == 11)) {
            if (birthMonth == 2 && !isLeapYear && birthDate > 28) {         // checks for february not during leapyear
                return false;
            } else if (birthMonth == 2 && isLeapYear && birthDate > 29) {   // checks for february during leapyear
                return false;
            } else if (birthMonth != 2 && birthDate > 30) {                // checks for months with 30 days
                return false;
            }
        } else {
            return false;       // if the birth month is not in the range from 1 to 12
        }

        // 5. check E/NPM Code: the result of the calculation below is the same as the digit of part E of NPM
        // get first 13 digits of NPM

        int[] digit = new int[13];          // put 13 digits in an array: e.g. 20022808200017 -> {2, 0, 0, ..., 0, 1}
        long temp = npm / 10;
        for (int i = 12; i >= 0; i--) {     // i starts from 12 because using npm % 10 -> starts from rightmost digit
            digit[i] = (int) (temp % 10);
            temp /= 10;
        }

        int check = 0;      // do the calculation
        for (int i = 0; i < 6; i++) {
            check += (digit[i] * digit[12 - i]);    // e.g. digit[0] * digit[12 - 0]
        }
        check += digit[6];

        while (check >= 10) {      // if the result is >= 10, add all of the digits together
            temp = check;
            check = 0;
            while (temp != 0) {
                check += temp % 10;         // e.g. temp = 73 -> check += 2 then check += 7 -> check = 9
                temp /= 10;
            }
        }

        return (check == npmCode);      // returns if check equals npmCode because last step of validation
    }

    public static String extract(long npm) {
        // TODO: Extract information from NPM, return string with given format
        String major, birthDateStr, majorStr, yearStr;
        int birthYear,birthMonth, birthDay, majorCode, yearAdmitted;

        // get the values for needed data
        birthYear = (int) ((npm / 100) % 10000);
        birthMonth = (int) ((npm / 1000000) % 100);
        birthDay = (int) ((npm / 100000000) % 100);
        majorCode = (int) ((npm / 10000000000L) % 100);
        yearAdmitted = (int) (2000 + ((npm / 1000000000000L) % 100));

        // find out which major the student is in
        switch (majorCode) {
            case 1 -> major = "Ilmu Komputer";
            case 2 -> major = "Sistem Informasi";
            case 3 -> major = "Teknologi Informasi";
            case 11 -> major = "Teknik Telekomunikasi";
            case 12 -> major = "Teknik Elektro";
            default -> major = "-";
        }

        // write the strings
        yearStr = "Tahun masuk: " + yearAdmitted;
        majorStr = "\nJurusan: " + major;

        // for cases when birth day/birth month is single digits
        if (birthDay < 10 && birthMonth < 10) {
            birthDateStr = "\nTanggal Lahir: 0" + birthDay + "-0" + birthMonth + "-" + birthYear;
        } else if (birthDay < 10) {
            birthDateStr = "\nTanggal Lahir: 0" + birthDay + "-" + birthMonth + "-" + birthYear;
        } else if (birthMonth < 10) {
            birthDateStr = "\nTanggal Lahir: " + birthDay + "-0" + birthMonth + "-" + birthYear;
        } else {
            birthDateStr = "\nTanggal Lahir: " + birthDay + "-" + birthMonth + "-" + birthYear;
        }

        return yearStr + majorStr + birthDateStr;
    }

    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        while (!exitFlag) {
            long npm = input.nextLong();
            if (npm < 0) {
                exitFlag = true;
                break;
            }

            // TODO: Check validate and extract NPM
            if (validate(npm)) {                          // the extraction method is called only if the npm is valid
                System.out.println(extract(npm));
            }
            else {
                System.out.println("NPM tidak valid!");
            }
        }
        input.close();
    }
}