package assignments.assignment2;

import java.util.Arrays;
import java.util.Scanner;

public class SistemAkademik {
    private static final int ADD_MATKUL = 1;
    private static final int DROP_MATKUL = 2;
    private static final int RINGKASAN_MAHASISWA = 3;
    private static final int RINGKASAN_MATAKULIAH = 4;
    private static final int KELUAR = 5;
    private static Mahasiswa[] daftarMahasiswa = new Mahasiswa[100];
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    private static int jumlahMahasiwa;
    private static int jumlahMataKuliah;
    
    private Scanner input = new Scanner(System.in);

    private Mahasiswa getMahasiswa(long npm) {
        /* TODO: Implementasikan kode Anda di sini */
        Mahasiswa tempMahasiswa = new Mahasiswa();
        for (int i = 0; i < jumlahMahasiwa; i++) {
            tempMahasiswa = daftarMahasiswa[i];
            if (tempMahasiswa.getNpm() == npm)
                break;
        }

        return tempMahasiswa;
    }

    private MataKuliah getMataKuliah(String namaMataKuliah) {
        /* TODO: Implementasikan kode Anda di sini */
        MataKuliah tempMatkul = new MataKuliah();
        for (int i = 0; i < jumlahMataKuliah; i++) {
            tempMatkul = daftarMataKuliah[i];
            if (tempMatkul.toString().equals(namaMataKuliah)) {
                break;
            }
        }

        return tempMatkul;
    }

    private void addMatkul(){
        System.out.println("\n--------------------------ADD MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan ADD MATKUL : ");
        long npm = Long.parseLong(input.nextLine());
        Mahasiswa mahasiswa = getMahasiswa(npm);


        System.out.print("Banyaknya Matkul yang Ditambah: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());

        System.out.println("Masukkan nama matkul yang ditambah");
        for(int i=0; i < banyakMatkul; i++){
            System.out.print("Nama matakuliah " + i+1 + " : ");
            String namaMataKuliah = input.nextLine();
            MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);

            // addMatkul will check if the student can add the course or not
            mahasiswa.addMatkul(mataKuliah);
        }

        System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
    }

    private void dropMatkul(){
        System.out.println("\n--------------------------DROP MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan DROP MATKUL : ");
        long npm = Long.parseLong(input.nextLine());
        Mahasiswa mahasiswa = getMahasiswa(npm);

        // if the student hasn't enrolled in any courses
        if (mahasiswa.getJumlahMataKuliah() == 0) {
            System.out.println("[DITOLAK] Belum ada mata kuliah yang diambil.");
            return;
        }

        System.out.print("Banyaknya Matkul yang Di-drop: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());

        System.out.println("Masukkan nama matkul yang di-drop:");
        for(int i=0; i < banyakMatkul; i++){
            System.out.print("Nama matakuliah " + i+1 + " : ");
            String namaMataKuliah = input.nextLine();
            MataKuliah tempMatkul = getMataKuliah(namaMataKuliah);

            // dropMatkul will check if the mahasiswa can drop it or not
            mahasiswa.dropMatkul(tempMatkul);

        }

        System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
    }

    private void ringkasanMahasiswa(){
        System.out.print("Masukkan npm mahasiswa yang akan ditunjukkan ringkasannya : ");
        long npm = Long.parseLong(input.nextLine());
        Mahasiswa mahasiswa = getMahasiswa(npm);


        // TODO: Isi sesuai format keluaran
        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama: " + mahasiswa.toString());
        System.out.println("NPM: " + npm);
        System.out.println("Jurusan: " + mahasiswa.getJurusan());
        System.out.println("Daftar Mata Kuliah: ");

        /* TODO: Cetak daftar mata kuliah 
        Handle kasus jika belum ada mata kuliah yang diambil*/
        MataKuliah[] daftarMatkul = mahasiswa.getDaftarMatkul();
        if (mahasiswa.getJumlahMataKuliah() != 0) {
            for (int i = 0, j = 0; i < mahasiswa.getJumlahMataKuliah(); i++) {
                if (daftarMatkul[i] != null)
                    System.out.println((j + 1) + ". " + daftarMatkul[i].toString());
                    j++;
            }
        } else {
            System.out.println("Belum ada mata kuliah yang diambil");
        }

        System.out.println("Total SKS: " + mahasiswa.getTotalSKS());
        
        System.out.println("Hasil Pengecekan IRS:");
        /* TODO: Cetak hasil cek IRS
        Handle kasus jika IRS tidak bermasalah */
        mahasiswa.cekIRS();
        String[] masalahIRS = mahasiswa.getMasalahIRS();
        int length = mahasiswa.getJumlahMasalah();


        if (length != 0) {
            for (int i = 0, j = 0; i < length; i++) {
                if (masalahIRS[i] != null) {
                    System.out.println((j + 1) + ". " + masalahIRS[i]);
                    j++;
                }
            }
        } else {
            System.out.println("IRS tidak bermasalah.");
        }
    }

    private void ringkasanMataKuliah(){
        System.out.print("Masukkan nama mata kuliah yang akan ditunjukkan ringkasannya : ");
        String namaMataKuliah = input.nextLine();

        // get the matakuliah from array
        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
        
        // TODO: Isi sesuai format keluaran
        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama mata kuliah: " + mataKuliah.toString());
        System.out.println("Kode: " + mataKuliah.getKode());
        System.out.println("SKS: " + mataKuliah.getSks());
        System.out.println("Jumlah mahasiswa: " + mataKuliah.getJumlahMahasiswa());
        System.out.println("Kapasitas: " + mataKuliah.getKapasitas());
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini: ");


        Mahasiswa[] daftarMahasiswa = mataKuliah.getDaftarMahasiswa();

        // if there are no students enrolled
        if (daftarMahasiswa[0] == null) {
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini.\n");
        }
        else {
            int jumlahMahsiswa = mataKuliah.getJumlahMahasiswa();
            for (int i = 0; i < jumlahMahsiswa; i++) {
                if (daftarMahasiswa[i] != null) {
                    Mahasiswa tempMahasiswa = daftarMahasiswa[i];
                    System.out.println((i + 1) + ". " + tempMahasiswa.toString());
                }
            }
        }
    }

    private void daftarMenu(){
        int pilihan = 0;
        boolean exit = false;
        while (!exit) {
            System.out.println("\n----------------------------MENU------------------------------\n");
            System.out.println("Silakan pilih menu:");
            System.out.println("1. Add Matkul");
            System.out.println("2. Drop Matkul");
            System.out.println("3. Ringkasan Mahasiswa");
            System.out.println("4. Ringkasan Mata Kuliah");
            System.out.println("5. Keluar");
            System.out.print("\nPilih: ");
            try {
                pilihan = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                continue;
            }
            System.out.println();
            if (pilihan == ADD_MATKUL) {
                addMatkul();
            } else if (pilihan == DROP_MATKUL) {
                dropMatkul();
            } else if (pilihan == RINGKASAN_MAHASISWA) {
                ringkasanMahasiswa();
            } else if (pilihan == RINGKASAN_MATAKULIAH) {
                ringkasanMataKuliah();
            } else if (pilihan == KELUAR) {
                System.out.println("Sampai jumpa!");
                exit = true;
            }
        }

    }


    private void run() {
        System.out.println("====================== Sistem Akademik =======================\n");
        System.out.println("Selamat datang di Sistem Akademik Fasilkom!");
        
        System.out.print("Banyaknya Matkul di Fasilkom: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        jumlahMataKuliah = banyakMatkul;
        System.out.println("Masukkan matkul yang ditambah");
        System.out.println("format: [Kode Matkul] [Nama Matkul] [SKS] [Kapasitas]");

        for(int i=0; i<banyakMatkul; i++){
            String[] dataMatkul = input.nextLine().split(" ", 4);
            int sks = Integer.parseInt(dataMatkul[2]);
            int kapasitas = Integer.parseInt(dataMatkul[3]);

            /* TODO: Buat instance mata kuliah dan masukkan ke dalam Array */
            MataKuliah tempMataKuliah = new MataKuliah(dataMatkul[0], dataMatkul[1], sks, kapasitas);
            daftarMataKuliah[i] = tempMataKuliah;
        }


        System.out.print("Banyaknya Mahasiswa di Fasilkom: ");
        int banyakMahasiswa = Integer.parseInt(input.nextLine());
        jumlahMahasiwa = banyakMahasiswa;
        System.out.println("Masukkan data mahasiswa");
        System.out.println("format: [Nama] [NPM]");

        for(int i=0; i<banyakMahasiswa; i++){
            String[] dataMahasiswa = input.nextLine().split(" ", 2);
            long npm = Long.parseLong(dataMahasiswa[1]);
            /* TODO: Buat instance mata kuliah dan masukkan ke dalam Array */
            Mahasiswa tempMahasiswa = new Mahasiswa(dataMahasiswa[0], npm);
            daftarMahasiswa[i] = tempMahasiswa;
        }

        daftarMenu();
        input.close();
    }

    public static void main(String[] args) {
        SistemAkademik program = new SistemAkademik();
        program.run();
    }


    
}
