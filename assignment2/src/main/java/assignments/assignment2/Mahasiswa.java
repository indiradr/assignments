package assignments.assignment2;

import java.util.Arrays;

public class Mahasiswa {
    private MataKuliah[] mataKuliah = new MataKuliah[10];
    private int jumlahMataKuliah;
    private String[] masalahIRS = new String[1];
    private int jumlahMasalah;
    private MataKuliah[] matkulBermasalah = new MataKuliah[1];
    private int jumlahMatkulBermasalah;
    private int totalSKS;
    private String nama;
    private String jurusan;
    private long npm;

    public Mahasiswa(String nama, long npm){

        // constructor for Mahasiswa class
        this.nama = nama;
        this.npm = npm;

        int kodeJurusan = (int) ((npm / 10000000000L) % 100);
        if (kodeJurusan == 1)
            this.jurusan = "Ilmu Komputer";
        else
            this.jurusan = "Sistem Informasi";
    }

    public Mahasiswa(){ }

    // getter methods
    public String toString() { return nama; }

    public long getNpm() { return npm;}

    public String getJurusan() { return jurusan; }

    public int getTotalSKS() { return totalSKS; }

    public MataKuliah[] getDaftarMatkul() { return mataKuliah; }

    public String[] getMasalahIRS() { return masalahIRS; }

    public int getJumlahMataKuliah() { return jumlahMataKuliah; }

    public int getJumlahMasalah() { return jumlahMasalah;}

    
    public void addMatkul(MataKuliah mataKuliah){
        /* TODO: implementasikan kode Anda di sini */

        // case where there hasn't been any courses added
        if (this.mataKuliah[0] == null ) {

            // only rejected when the course is at full capacity
            if (mataKuliah.getJumlahMahasiswa() == mataKuliah.getKapasitas()) {
                System.out.println("[DITOLAK] " + mataKuliah + " telah penuh kapasitasnya.");
                return;
            }
            this.mataKuliah[0] = mataKuliah;
            jumlahMataKuliah++;
            totalSKS += mataKuliah.getSks();
            mataKuliah.addMahasiswa(this);  // add the student to the course
            return;
        }


        if (Arrays.asList(this.mataKuliah).contains(mataKuliah)) {  // check if student is already enrolled
            System.out.println("[DITOLAK] " + mataKuliah + " telah diambil sebelumnya.");
        }
        else { // check if
            if (mataKuliah.getJumlahMahasiswa() == mataKuliah.getKapasitas()) {
                System.out.println("[DITOLAK] " + mataKuliah + " telah penuh kapasitasnya.");
            }
            else {
                if (jumlahMataKuliah == 10) {   // check if the number of courses taken is equal to 10
                    System.out.println("[DITOLAK] Maksimal mata kuliah yang diambil hanya 10.");
                }
                else {
                    this.mataKuliah[jumlahMataKuliah] = mataKuliah; // add to mataKuliah array
                    jumlahMataKuliah++;
                    totalSKS += mataKuliah.getSks();
                    mataKuliah.addMahasiswa(this); // add the student to the course
                }
            }
        }

    }

    public void dropMatkul(MataKuliah mataKuliah){

        // checks if student is enrolled in the course or not
        boolean test = Arrays.asList(this.mataKuliah).contains(mataKuliah);
        if (!test) {
            System.out.println("[DITOLAK] " +  mataKuliah + " belum pernah diambil.");
            return;
        }

        MataKuliah[] temp = Arrays.copyOf(this.mataKuliah, this.mataKuliah.length);

        // if the course is in the middle of the list, shift the courses after to the left
        for (int i = 0, j = 0; i < jumlahMataKuliah; i++) {
            if (!temp[i].toString().equals(mataKuliah.toString())){
                this.mataKuliah[j] = temp[i];
                j++;
            }
        }
        jumlahMataKuliah--;
        totalSKS -= mataKuliah.getSks();
        this.mataKuliah[jumlahMataKuliah] = null;
        mataKuliah.dropMahasiswa(this); // remove student from course

    }


    public void cekIRS(){

        // when the courses taken did not match the major
        // check if the matakuliah array is null of not
        if (jumlahMataKuliah != 0) {
            for (int i = 0; i < jumlahMataKuliah; i++) {
                MataKuliah tempMatkul = mataKuliah[i];

                // check if the Matkul is problematic or not
                String kodeMatkul = tempMatkul.getKode();

                // problematic but masalahIRS is empty (SI)
                if (kodeMatkul.equals("IK") && jurusan.equals("Sistem Informasi") && masalahIRS[0] == null) {
                    masalahIRS[0] = "Mata Kuliah " + tempMatkul.toString() + " tidak dapat diambil jurusan SI";
                    jumlahMasalah++;
                    matkulBermasalah[0] = tempMatkul;
                    jumlahMatkulBermasalah++;
                }

                // problematic and masalahIRS is not empty (SI)
                else if (kodeMatkul.equals("IK") && jurusan.equals("Sistem Informasi")) {

                    // make copy so that masalahIRS and matkulBermasalah size grows by 1
                    if (!Arrays.asList(masalahIRS).contains("Mata Kuliah " + tempMatkul.toString() + " tidak dapat diambil jurusan SI")) {
                        masalahIRS = Arrays.copyOf(masalahIRS, masalahIRS.length + 1);
                        matkulBermasalah = Arrays.copyOf(matkulBermasalah, matkulBermasalah.length + 1);

                        // append to arrays
                        masalahIRS[jumlahMasalah] = "Mata Kuliah " + tempMatkul.toString() + " tidak dapat diambil jurusan SI";
                        jumlahMasalah++;
                        matkulBermasalah[jumlahMatkulBermasalah] = tempMatkul;
                        jumlahMatkulBermasalah++;
                    }
                }

                // problematic but masalahIRS is empty (IK)
                if (kodeMatkul.equals("SI") && jurusan.equals("Ilmu Komputer") && masalahIRS[0] == null) {
                    masalahIRS[0] = "Mata Kuliah " + tempMatkul.toString() + " tidak dapat diambil jurusan IK";
                    jumlahMasalah++;
                    matkulBermasalah[0] = tempMatkul;
                    jumlahMatkulBermasalah++;
                }

                // problematic and masalahIRS is not empty (IK
                else if (kodeMatkul.equals("SI") && jurusan.equals("Ilmu Komputer")) {
                    if (!Arrays.asList(masalahIRS).contains("Mata Kuliah " + tempMatkul.toString() + " tidak dapat diambil jurusan IK")) {

                        // make copy so that masalahIRS and matkulBermasalah size grows by 1
                        masalahIRS = Arrays.copyOf(masalahIRS, masalahIRS.length + 1);
                        matkulBermasalah = Arrays.copyOf(matkulBermasalah, matkulBermasalah.length + 1);

                        // append to arrays
                        masalahIRS[jumlahMasalah] = "Mata Kuliah " + tempMatkul.toString() + " tidak dapat diambil jurusan SI";
                        jumlahMasalah++;
                        matkulBermasalah[jumlahMatkulBermasalah] = tempMatkul;
                        jumlahMatkulBermasalah++;
                    }
                }

            }
        }

        // when the totalSKS is over 24
        if (totalSKS > 24) {

            // this problem is prioritized, so it should show up first.
            if (masalahIRS[0] == null) {
                masalahIRS[0] = "SKS yang Anda ambil lebih dari 24";
                jumlahMasalah++;


            // if there are other problems, shift the others to the right
            } else if (!masalahIRS[0].equals("SKS yang Anda ambil lebih dari 24")) {

                // make copy so that masalahIRS and matkulBermasalah size grows by 1
                masalahIRS = Arrays.copyOf(masalahIRS, masalahIRS.length + 1);
                String[] tempMasalah = Arrays.copyOf(masalahIRS, masalahIRS.length);
                jumlahMasalah++;
                for (int i = 0, j = 0; i < jumlahMasalah - 1; i++) {
                    masalahIRS[i + 1] = tempMasalah[j]; // shift all the previous problems
                    j++;
                }
                masalahIRS[0] = "SKS yang Anda ambil lebih dari 24"; // make it the first problem that shows up

            }

        }

        // if the SKS problem is resolved, remove from masalahIRS
        else if (masalahIRS[0] != null && Arrays.asList(masalahIRS).contains("SKS yang Anda ambil lebih dari 24")) {
            String[] temp = Arrays.copyOf(masalahIRS, jumlahMasalah);
            for (int i = 0, j = 0; i < jumlahMasalah; i++) {
                if (!temp[i].equals("SKS yang Anda ambil lebih dari 24")){
                    masalahIRS[j] = temp[i];
                    j++;
                }
            }
            jumlahMasalah--;

            // decrease the size of array by 1
            if (jumlahMasalah != 0) {
                masalahIRS = Arrays.copyOf(masalahIRS, jumlahMasalah);
            }
            else {  // but make sure the minimum length is 1
                masalahIRS[0] = null;
            }
        }


        // if there are problematic courses, check if they've been removed or not
        if (jumlahMatkulBermasalah != 0) {
            int tempJumlah = jumlahMatkulBermasalah;
            for (int i = 0; i < tempJumlah; i++) {
                MataKuliah tempProblematic = matkulBermasalah[i];

                // if the problematic course has been removed, remove from masalahIRS and matkulBermasalah
                if (!Arrays.asList(mataKuliah).contains(tempProblematic)) {
                    String[] tempMasalahIRS = Arrays.copyOf(masalahIRS, jumlahMasalah);
                    for (int j = 0, k = 0; j < jumlahMasalah; j++) {
                        String[] tempMasalah = tempMasalahIRS[j].split(" ");

                        // add all of the problems back to the list, except the string containing removed matkul
                        if (!tempMasalah[2].equals(tempProblematic.toString())) {
                            masalahIRS[k] = tempMasalahIRS[j];
                            k++;
                        }
                    }
                    jumlahMasalah--;

                    MataKuliah[] tempMatkulBermasalah = Arrays.copyOf(matkulBermasalah, jumlahMatkulBermasalah);
                    for (int j = 0, k = 0; j < jumlahMatkulBermasalah; j++) {
                        // add all of the problems back to the list, except the removed matkul
                        if (!tempMatkulBermasalah[j].toString().equals(tempProblematic.toString())) {
                            matkulBermasalah[k] = tempMatkulBermasalah[j];
                            k++;
                        }
                    }
                    jumlahMatkulBermasalah--;
                }
            }

            // decrease the size of array by 1
            if (jumlahMasalah != 0) {
                masalahIRS = Arrays.copyOf(masalahIRS, jumlahMasalah);
            }

            // but make sure the minimum length is 1
            else {
                masalahIRS = Arrays.copyOf(masalahIRS, 1);
                masalahIRS[0] = null;
            }

            // decrease the size of array by 1
            if (jumlahMatkulBermasalah != 0) {
                matkulBermasalah = Arrays.copyOf(matkulBermasalah, jumlahMatkulBermasalah);
            }

            // but make sure the minimum length is 1
            else {
                matkulBermasalah = Arrays.copyOf(matkulBermasalah, 1);
                matkulBermasalah[0] = null;
            }
        }
    }



}
