package assignments.assignment2;

import java.util.Arrays;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private int jumlahMahasiswa;
    private Mahasiswa[] daftarMahasiswa;

    // constructor methods
    public MataKuliah(String kode, String nama, int sks, int kapasitas){
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.kode = kode;
        this.sks = sks;
        this.kapasitas = kapasitas;

        // the array daftarMahasiswa's size is the capacity of the course
        this.daftarMahasiswa = new Mahasiswa[kapasitas];
    }

    public MataKuliah(){
    }

    // getter methods
    public String toString() { return nama; }

    public Mahasiswa[] getDaftarMahasiswa() { return daftarMahasiswa; }

    public int getJumlahMahasiswa() { return jumlahMahasiswa; }

    public int getKapasitas() {
        return kapasitas;
    }

    public String getKode() {
        return kode;
    }

    public int getSks() {
        return sks;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {

        // checking if the student is eligible to take the course has already been checked in Mahasiswa class
        // so just add the student in the course
        daftarMahasiswa[jumlahMahasiswa] = mahasiswa;
        jumlahMahasiswa++;
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        if (daftarMahasiswa != null) {

            // check if the student is in the list
            boolean test = Arrays.asList(daftarMahasiswa).contains(mahasiswa);
            if (test) {

                // shift the student to the left if the dropped student is in the middle of array
                Mahasiswa[] temp = Arrays.copyOf(daftarMahasiswa, daftarMahasiswa.length);
                for (int i = 0, j = 0; i < jumlahMahasiswa; i++) {
                    if (!temp[i].toString().equals(mahasiswa.toString())){
                        daftarMahasiswa[j] = temp[i];
                        j++;
                    }

                }
                jumlahMahasiswa--;
                daftarMahasiswa[jumlahMahasiswa] = null;


            }

        }

    }


}
